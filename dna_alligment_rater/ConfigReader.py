class NeedlemanWunschConfig:
    GAP_PENALTY = None
    SAME = None
    DIFF = None
    MAX_SEQ_LENGTH = None
    MAX_NUMBER_PATHS = None


def ReadConfig(filename):
    config = NeedlemanWunschConfig()
    with open(filename) as fileHandle:
        line = fileHandle.readline()
        while line:
            (key, valueString) = line.split(" ", 2)
            value = int(valueString)
            if key == "GAP_PENALTY":
                config.GAP_PENALTY = value
            elif key == "SAME":
                config.SAME = value
            elif key == "DIFF":
                config.DIFF = value
            elif key == "MAX_SEQ_LENGTH":
                if value <= 0:
                    raise ValueError("MAX_SEQ_LENGTH Has to be larger than 0")
                config.MAX_SEQ_LENGTH = value
            elif key == "MAX_NUMBER_PATHS":
                if value <= 0:
                    raise ValueError("MAX_NUMBER_PATHS Has to be larger than 0")
                config.MAX_NUMBER_PATHS = value
            else:
                raise ValueError("Invalid config file")
            line = fileHandle.readline()

    if config.GAP_PENALTY == None:
        raise ValueError("GAP_PENALTY was not provided")
    if config.SAME == None:
        raise ValueError("SAME was not provided")
    if config.DIFF == None:
        raise ValueError("DIFF was not provided")
    if config.MAX_SEQ_LENGTH == None:
        raise ValueError("MAX_SEQ_LENGTH was not provided")
    if config.MAX_NUMBER_PATHS == None:
        raise ValueError("MAX_NUMBER_PATHS was not provided")
    
    return config
