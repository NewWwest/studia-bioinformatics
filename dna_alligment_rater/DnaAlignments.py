import numpy as np
import sys
import getopt
import json


class DnaAlignments(object):
    def __init__(self, maxSolutions):
        self.MaxSolutions = maxSolutions
        self._solutions = []
        self._branchPoints = []

    def fromNeedlemanWunschMatrix(self, needlemanWunschMatrix):
        self._matrix = needlemanWunschMatrix
        self._score = self._matrix.score()

        iStart = len(self._matrix.matrix) - 1
        jStart = len(self._matrix.matrix[0]) - 1
        self._branchPoints.append(("", "", iStart, jStart))

        while len(self._branchPoints) != 0 and len(self._solutions) < self.MaxSolutions:
            d1, d2, i, j = self._branchPoints.pop()
            self._processBranchPoint((d1, d2), i, j)

        return self

    def _processBranchPoint(self, solutionBegginng, i, j):
        if i == 0 and j == 0:
            self._solutions.append(
                (solutionBegginng[0][::-1], solutionBegginng[1][::-1]))
            return

        dna1Alligned = solutionBegginng[0]
        dna2Alligned = solutionBegginng[1]

        while i != 0 or j != 0:
            #pIndex!=0 (branches)
            for pIndex in range(1, len(self._matrix.matrix[i][j].Path)):
                directionIdentifier = self._matrix.matrix[i][j].Path[pIndex]
                if directionIdentifier == "L":
                    d1 = dna1Alligned + self._matrix.dna1[j-1]
                    d2 = dna2Alligned + "-"
                    self._branchPoints.append((d1, d2, i, j-1))
                elif directionIdentifier == "T":
                    d1 = dna1Alligned + "-"
                    d2 = dna2Alligned + self._matrix.dna2[i-1]
                    self._branchPoints.append((d1, d2, i-1, j))
                elif directionIdentifier == "D":
                    d1 = dna1Alligned + self._matrix.dna1[j-1]
                    d2 = dna2Alligned + self._matrix.dna2[i-1]
                    self._branchPoints.append((d1, d2, i-1, j-1))
            #pIndex==0 (mainPath)
            directionId = self._matrix.matrix[i][j].Path[0]
            if directionId == "L":
                dna1Alligned += self._matrix.dna1[j-1]
                dna2Alligned += "-"
                j -= 1
            elif directionId == "T":
                dna1Alligned += "-"
                dna2Alligned += self._matrix.dna2[i-1]
                i -= 1
            elif directionId == "D":
                dna1Alligned += self._matrix.dna1[j-1]
                dna2Alligned += self._matrix.dna2[i-1]
                j -= 1
                i -= 1

        self._solutions.append((dna1Alligned[::-1], dna2Alligned[::-1]))

    def save(self, filename):
        with open(filename, "w+") as fileHandle:
            fileHandle.write("Score: {}".format(self._score))
            fileHandle.write("\n")
            for i in range(0, len(self._solutions)):
                sol = self._solutions[i]
                fileHandle.write("==={}===".format(i+1))
                fileHandle.write("\n")
                fileHandle.write(sol[0])
                fileHandle.write("\n")
                fileHandle.write(sol[1])
                fileHandle.write("\n")
