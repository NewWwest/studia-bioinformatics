def ReadFasta(filename):
    result=""
    with open(filename) as fileHandle:
        line = fileHandle.readline()
        while line:
            if line.startswith(">") or line.startswith(";"):
                line = fileHandle.read()
                continue

            for symbol in line:
                if not symbol.isspace():
                    result+=symbol
            
            line = fileHandle.read()
    return result