class MatrixNode:
    def __init__(self):
        self.Value = 0
        self.Path = []


class NeedlemanWunschMatrix(object):
    def __init__(self, firstDna, secondDna, gapPenalty, differentPenalty, sameReward):
        self.dna1 = firstDna
        self.dna2 = secondDna
        self.gapPenalty = gapPenalty
        self.differentPenalty = differentPenalty
        self.sameReward = sameReward

        self._columns = len(self.dna1)+1
        self._rows = len(self.dna2)+1
        self.matrix = [[MatrixNode() for j in range(self._columns)]
                       for i in range(self._rows)]

        self.process()

    def process(self):
        self.matrix[0][0].Value = 0

        for i in range(1, self._rows):
            self.matrix[i][0].Value = (self.gapPenalty*i)
            self.matrix[i][0].Path.append("T")

        for j in range(1, self._columns):
            self.matrix[0][j].Value = (self.gapPenalty*j)
            self.matrix[0][j].Path.append("L")

        for i in range(1, self._rows):
            for j in range(1, self._columns):
                top = self.matrix[i-1][j].Value + self.gapPenalty
                left = self.matrix[i][j-1].Value + self.gapPenalty
                diag = 0
                if (self.dna2[i-1] == self.dna1[j-1]):
                    diag = self.sameReward + self.matrix[i-1][j-1].Value
                else:
                    diag = self.differentPenalty + self.matrix[i-1][j-1].Value

                maxf = max(top, left, diag)
                if(maxf == top):
                    self.matrix[i][j].Path.append("T")
                if(maxf == left):
                    self.matrix[i][j].Path.append("L")
                if(maxf == diag):
                    self.matrix[i][j].Path.append("D")
                self.matrix[i][j].Value = maxf

    def saveMatrix(self, fileName):
        with open(fileName, "w+") as fileHandle:
            for i in range(len(self.matrix)):
                for j in range(len(self.matrix[i])):
                    fileHandle.write('{} ({});\t'.format(
                        self.matrix[i][j].Value, self.matrix[i][j].Path))
                fileHandle.write("\n")

    def score(self):
        return self.matrix[len(self.matrix)-1][len(self.matrix[0])-1].Value
