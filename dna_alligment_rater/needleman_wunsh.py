import numpy as np
import sys
import getopt
import json
import NeedlemanWunschMatrix
import DnaAlignments
import FastaReader
import ConfigReader

SAVE_MATRIX = True


def readArguments(argv):
    usage = "main.py -a <inputFile1> -b <inputFile2> -c <configFile> -o <outputfile>"
    inputFile1 = ""
    inputFile2 = ""
    configFile = ""
    outputFile = ""
    try:
        opts, _ = getopt.getopt(argv, "ha:b:c:o:",)
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print(usage)
            sys.exit()
        elif opt == "-a":
            inputFile1 = arg
        elif opt in ("-b"):
            inputFile2 = arg
        elif opt in ("-c"):
            configFile = arg
        elif opt in ("-o"):
            outputFile = arg

    if configFile == "":
        configFile = "defaultConfig.txt"
    if outputFile == "":
        outputFile = "output.txt"

    return (inputFile1, inputFile2, configFile, outputFile)


def main(inputFile1, inputFile2, configFile, outputFile):
    dna1 = FastaReader.ReadFasta(inputFile1)
    dna2 = FastaReader.ReadFasta(inputFile2)
    data = ConfigReader.ReadConfig(configFile)
    if data.MAX_SEQ_LENGTH < len(dna1) or data.MAX_SEQ_LENGTH < len(dna2):
        raise ValueError("Dna Sequence too long")

    scores = NeedlemanWunschMatrix.NeedlemanWunschMatrix(
        dna1, dna2, data.GAP_PENALTY, data.DIFF, data.SAME)

    if SAVE_MATRIX:
        scores.saveMatrix(outputFile+".matrix")

    dnaAlignments = DnaAlignments.DnaAlignments(
        data.MAX_NUMBER_PATHS).fromNeedlemanWunschMatrix(scores)

    dnaAlignments.save(outputFile)


if __name__ == "__main__":
    (inputFile1, inputFile2, configFile,
     outputFile) = readArguments(sys.argv[1:])
    main(inputFile1, inputFile2, configFile, outputFile)
