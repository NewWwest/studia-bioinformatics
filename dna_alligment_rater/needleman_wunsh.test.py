import needleman_wunsh
import unittest

resultFilename = "out.temp"


class TestStringMethods(unittest.TestCase):
    def test_When_Dna_match_score_should_be_length_times_same_reward(self):
        needleman_wunsh.main("resources/test1a.txt",
                             "resources/test1a.txt", "resources/testConfigDefault.txt", resultFilename)
        with open(resultFilename) as fileHandle:
            line = fileHandle.readline()
            (_, valueString) = line.split(" ", 2)
            value = int(valueString)

        self.assertEqual(value, 60)

    def test_When_Dna_dont_match_score_should_be_smaller_than_when_they_match(self):
        needleman_wunsh.main("resources/test1a.txt",
                             "resources/test1b.txt", "resources/testConfigDefault.txt", resultFilename)
        with open(resultFilename) as fileHandle:
            line = fileHandle.readline()
            (_, valueString) = line.split(" ", 2)
            value = int(valueString)

        self.assertLess(value, 60)

    def test_There_should_be_4_solutions_all_with_gaps(self):
        needleman_wunsh.main("resources/test1a.txt",
                             "resources/test1b.txt", "resources/testConfigDefault.txt", resultFilename)
        with open(resultFilename) as fileHandle:
            line = fileHandle.readline()  # score line
            line = fileHandle.readline()  # first line
            counter = 0
            while line:
                if line.startswith("="):
                    line = fileHandle.readline()
                elif "-" in line:
                    counter += 1
                line = fileHandle.readline()

        self.assertEqual(counter, 4)

    def test_There_should_be_no_gaps_instead_there_should_be_different(self):
        needleman_wunsh.main("resources/test1a.txt", "resources/test1b.txt",
                             "resources/testConfigLowDiffPenalty.txt", resultFilename)
        with open(resultFilename) as fileHandle:
            line = fileHandle.readline()  # score line
            line = fileHandle.readline()  # first line
            while line:
                if line.startswith("="):
                    line = fileHandle.readline()

                self.assertFalse("-" in line)
                line = fileHandle.readline()

    def test_When_input_is_the_same_the_result_should_be_same_dna_printed_2_times(self):
        needleman_wunsh.main("resources/test1a.txt",
                             "resources/test1a.txt", "resources/testConfigDefault.txt", resultFilename)
        with open(resultFilename) as fileHandle:
            fileHandle.readline()  # score
            fileHandle.readline()  # ===
            dna1 = fileHandle.readline()  # dna1
            dna2 = fileHandle.readline()  # dna2

        self.assertEqual(dna2, dna1)

    def test_Script_should_thorw_when_input_is_too_big(self):
        try:
            needleman_wunsh.main("resources/test1a.txt",
                                 "resources/tes4ab.txt", "resources/testConfigDefault.txt", resultFilename)
            self.assertTrue(False)
        except:
            pass
        try:
            needleman_wunsh.main("resources/tes4ab.txt",
                                 "resources/test1a.txt", "resources/testConfigDefault.txt", resultFilename)
            self.assertTrue(False)
        except:
            pass

    def test_There_should_be_only_20_solution_and_40_lines(self):
        print("running test 7")
        needleman_wunsh.main("resources/test5a.txt",
                             "resources/test5a.txt", "resources/testConfigDefault.txt", resultFilename)
        with open(resultFilename) as fileHandle:
            line = fileHandle.readline()  # score line
            line = fileHandle.readline()  # first line
            counter = 0
            while line:
                if line.startswith("="):
                    line = fileHandle.readline()

                counter += 1
                line = fileHandle.readline()

        self.assertLessEqual(counter, 40)


if __name__ == '__main__':
    unittest.main()
