from model_creation import loadFile, score_chromosome, to_single_dimension_array, learn
import matplotlib.pyplot as plt
import pickle

USE_SAVED_MODEL = False
START_LINE = 100000  # Around this line the N's end
LAST_LINE = None  # ending N's begin like 20 lines before end, so lets not bother
SHOW_PLOT = False  # Really makes sense below 10k lines
VERBOSE = True

SPAN =1500
STEP=750

def getChunks(filename):
    if VERBOSE:
        print("Chunking started.")
    lines = loadFile(filename)
    data = ''.join(lines[START_LINE:LAST_LINE])
    chunks = []
    i = 0
    while True:
        newChunk = data[i:i+SPAN]
        if len(newChunk) < SPAN:
            break
        chunks.append(newChunk)
        i += STEP

    return chunks


def rate(chunks, clasifier):
    if VERBOSE:
        print("Chunk rating started.")
    data = []
    dataIndexes = {}
    j = 0
    for i in range(0, len(chunks)):
        if 'N' not in chunks[i]:
            values = score_chromosome(chunks[i])
            linear = to_single_dimension_array(values)
            data.append(linear)
            dataIndexes[i] = j
            j += 1

    if VERBOSE:
        print("Evaluation started.")
    result = clasifier.predict_proba(data)
    avg = (sum(result)/len(result))[0]

    if VERBOSE:
        print(f"Chromosome score is {avg}.")
        print("Gap filling started.")
    final = []
    for i in range(0, len(chunks)):
        if 'N' not in chunks[i]:
            final.append(result[dataIndexes[i]][0])
        else:
            final.append(avg)

    return final


def save_to_wig(data, filename, chromosome, start, step, span):
    if VERBOSE:
        print("Saving WIG file started.")
    with open(filename, 'w') as f:
        f.write('track type=wiggle_0 name="db=vista tissue=both histmods= kmers=4mers '+
         'chrom=chr21.id" description="enhancers prediction" visibility=full autoScale=off '+
         'vieLimits=0.0:1.0 color=50,150,255 yLineMark=11.76 yLineOnOff=on priority=10\n')
        f.write(
            f'fixedStep chrom={chromosome} start={start} step={step} span={span}\n')
        for value in data:
            f.write("{0:.4f}\n".format(value))


if __name__ == "__main__":
    chunks = getChunks("resources/chr21.fa")
    if USE_SAVED_MODEL:
        clf = pickle.load(open('model.data', 'rb'))
    else:
        clf = learn("resources/vista1500.txt", "resources/randoms1500.txt")

    result = rate(chunks, clf)
    if SHOW_PLOT:
        plt.plot(result, 'ro')
        plt.show()

    save_to_wig(result, 'chr21.wig', 'chr21', START_LINE*50, STEP, SPAN)
