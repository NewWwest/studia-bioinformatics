from sklearn.ensemble import RandomForestClassifier
import itertools
import csv
import pickle
K_MER = 4

def reverse_complement(xd):
    result = []
    for i in range(0, K_MER):
        if xd[i] == "A":
            result.append("T")
        elif xd[i] == "T":
            result.append("A")
        elif xd[i] == "G":
            result.append("C")
        elif xd[i] == "C":
            result.append("G")
    return "".join(list(reversed(result)))


def generateKmers():
    result = []
    resultRejected = []
    arrays = list(itertools.product(["A", "T", "C", "G"], repeat=K_MER))
    for i in range(0, 256):
        kmer = "".join(arrays[i])
        rc = reverse_complement(arrays[i])
        if kmer not in resultRejected:
            result.append(kmer)
            resultRejected.append(rc)

    return result


def loadFile(filename):
    result = []
    with open(filename) as fileHandle:
        line = fileHandle.readline()
        while line:
            if not line.startswith(">"):
                result.append(line.upper().strip())
            line = fileHandle.readline()

    return result

Kmer_dictionary = None
def get_Kmer_dictionary():
    global Kmer_dictionary
    if not Kmer_dictionary:
        Kmer_dictionary = {i: 0 for i in generateKmers()}
    
    return Kmer_dictionary.copy()

def score_chromosome(data):
    result = get_Kmer_dictionary()
    for i in range(0, len(data)-K_MER):
        current = data[i:i+K_MER]
        if current in result:
            result[current] += 1
        else:
            rc = reverse_complement(current)
            if rc in result:
                result[rc] += 1
    return result

def to_single_dimension_array(dic):
    result = []
    for kv in sorted(dic.keys()):
        result.append(dic[kv])
    return result


def generate_training_data(positivesFilename, negativesFilename):
    result = []
    labels = []
    data = loadFile(positivesFilename)
    for d in data:
        scores = score_chromosome(d)
        labels.append(1)
        result.append(scores)
    data = loadFile(negativesFilename)

    for d in data:
        scores = score_chromosome(d)
        labels.append(0)
        result.append(scores)

    return (result, labels)


def learn(positivesFilename, negativesFilename):
    (dicts, labels) = generate_training_data(positivesFilename, negativesFilename)
    values = []

    for i in range(0, len(dicts)):
        values.append(to_single_dimension_array(dicts[i]))

    clf = RandomForestClassifier(n_estimators=10)
    clf = clf.fit(values, labels)
    return clf


def save_model(model, filename):
    with open(filename, 'wb') as f:
        pickle.dump(clf, f)


if __name__ == "__main__":
    clf = learn("resources/vista1500.txt","resources/randoms1500.txt")
    save_model(clf, "model.data")