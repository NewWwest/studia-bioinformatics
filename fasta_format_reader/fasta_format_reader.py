#A->U
#T->A
#G->C
#C->G

map = {"UUU":"F", "UUC":"F", "UUA":"L", "UUG":"L",
    "UCU":"S", "UCC":"s", "UCA":"S", "UCG":"S",
    "UAU":"Y", "UAC":"Y", "UAA":"STOP", "UAG":"STOP",
    "UGU":"C", "UGC":"C", "UGA":"STOP", "UGG":"W",
    "CUU":"L", "CUC":"L", "CUA":"L", "CUG":"L",
    "CCU":"P", "CCC":"P", "CCA":"P", "CCG":"P",
    "CAU":"H", "CAC":"H", "CAA":"Q", "CAG":"Q",
    "CGU":"R", "CGC":"R", "CGA":"R", "CGG":"R",
    "AUU":"I", "AUC":"I", "AUA":"I", "AUG":"M",
    "ACU":"T", "ACC":"T", "ACA":"T", "ACG":"T",
    "AAU":"N", "AAC":"N", "AAA":"K", "AAG":"K",
    "AGU":"S", "AGC":"S", "AGA":"R", "AGG":"R",
    "GUU":"V", "GUC":"V", "GUA":"V", "GUG":"V",
    "GCU":"A", "GCC":"A", "GCA":"A", "GCG":"A",
    "GAU":"D", "GAC":"D", "GAA":"E", "GAG":"E",
    "GGU":"G", "GGC":"G", "GGA":"G", "GGG":"G",}


def DnaToRna(inputx, outputx):
    fn=open(outputx, "w")
    f=open(inputx, "r")
    f.readline()

    while True:
        symbol = f.read(1)
        if   symbol=='A':
            fn.write("U")
        elif symbol=="T":
            fn.write("A")
        if   symbol=="G":
            fn.write("C")
        elif symbol=="C":
            fn.write("G")
        else:
            fn.write(symbol)
            
        if not symbol:
            break 

def RnaToProtein(input, output):
    fn=open(output, "w")
    f=open(input, "r")

    while True:
        triplet = ""
        while len(triplet)<3:
            symbol = f.read(1)
            if symbol == "G" or symbol == "C" or symbol == "U" or symbol == "A":
                triplet +=symbol
            if not symbol:
                break 
        
        if not symbol:
            break 

        fn.write(map[triplet])



def CountGandC(input, output):
    fn=open(output, "w")
    fn.write("start\tstop\tvalue\n")
    f=open(input, "r")
    f.readline()
    i=0
    while True:
        f.seek(i)
        symbol = f.read(100)
        count = symbol.count("G") + symbol.count("C")

        fn.write("{}\t{}\t{}\n".format(i,i+99,count))
        i=i+50
        if not symbol:
            break 

DnaToRna("sequence.fasta", "sequenceRNA.fasta")
CountGandC("sequence.fasta", "GCCount.tsv")
RnaToProtein("sequenceRNA.fasta", "sequenceProtein.fasta")



